<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    protected $table = "products";
    protected $primaryKey = "id";
    protected $fillable = array('serial_number','ticket','brand','description','status','details');
   
    public function warranty()
    {
        return $this->belongsTo('Warranty');
    }
}
