<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    //$table->increments('id');
    // $table->string('order');
    // $table->string('bill');
    protected $table = "warranties";
    protected $primaryKey = "id";
    protected $fillable = array('order','active');

    protected $hiden = ['created_at','updated_at'];
    public function products()
    {
        return $this->hasMany('Product');
    }
}
