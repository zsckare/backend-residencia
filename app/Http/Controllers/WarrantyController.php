<?php

namespace App\Http\Controllers;

use App\Warranty;
use App\Product;
use Illuminate\Http\Request;

class WarrantyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['warranties' =>Warranty::all()],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rqst = $request->all();
        $rqst['active'] = 1;//se asign default al guardar la orden
        if (!$request->get('order')) {
            return response()->json(['mensaje'=>"Datos Invalidos"],202);    
        }

        $response = Warranty::create($rqst);
        $arr = array( );
        $array = json_decode($rqst['products'],true);
        // aqui se hace un foreach para guardar los productos que vengan en el parametro products de la peticion 
        foreach ($array as $key) {
            // array_push($arr,$response['id']);
            $p = new Product(); //se crea instancia del modelo Product
            $p->serial_number = $key['serial_number'];
            $p->status = 1;
            $p->ticket = $key['ticket'];
            $p->brand = $key['brand'];
            $p->description = $key['description'];
            // $p->details = $key['details'];
            $p->details = "krkmrk";
            $p->brand = $key['brand'];
            $p->warranty_id = $response['id'];
            $p->save();
            
        }
        
        return response()->json(['mensaje'=>"Se ha creado la orden de garantia", "datos"=>$response],202);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Warranty  $warranty
     * @return \Illuminate\Http\Response
     */
    public function show(Warranty $warranty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Warranty  $warranty
     * @return \Illuminate\Http\Response
     */
    public function edit(Warranty $warranty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Warranty  $warranty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Warranty $warranty)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Warranty  $warranty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warranty $warranty)
    {
        //
    }
}
